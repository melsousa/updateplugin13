<!doctype html>
		<html lang="en">
  		<head>
    		<meta charset="utf-8">
    		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    		<meta name="description" content="">
    		<meta name="author" content="">

   			<title>Spobrefy</title>
    		<link href="css/bootstrap.min.css" rel="stylesheet">
   			<link href="css/carousel.css" rel="stylesheet">

   		 </head>
   		<body style="background-color: rgb(30,30,46);">	
   			<header>
   				<!--menu-->
   				<nav class="navbar navbar-expand-md navbar-dark fixed-top " style="background-color: rgb(39,41,61);">
        			<img  width="60px" height="60px" class="imgmel" src="imagens/logo.jpg"/>

        			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
          			<span class="navbar-toggler-icon"></span>
        			</button>

        			<div class="collapse navbar-collapse" id="navbarCollapse">

	          			<ul class="navbar-nav mr-auto">
				            <li class="nav-item active">
				              	<a class="nav-link" href="novo.html">Início <span class="sr-only">(current)</span></a>
				            </li>
				            <li class="nav-item active">
				             	<a class="nav-link" href="artista.html">Artistas</a>
				            </li>
				            <li class="nav-item active">
		             		 	<a class="nav-link" href="register.php">Inscrever-se</a>
		            		</li>
		            		<li class="nav-item active">
		              			<a class="nav-link" href="user.php">Entrar</a>
		            		</li>
	          			</ul>

        			</div>
      			</nav>
      			<!--menu-->
   			</header>


   			<div>
   				<!--jumbotron-->
   				<div class="jumbotron jumbotron-fluid" style="background-color: rgb(30,30,46); ">
   					<div class="container">
   						<div class="card" style="background-color: rgb(39,41,61); ">
   							<div class="row">
   								<div class="col-12">
   									<div class="card-header">
   										<div class="row">
   											<div class="col-sm-6 text-left">
   												<h5 class="card-category">
   													Cards Musicais
   												</h5>
   												
   											</div>


   											<div class="col-sm-6">

   												<div class=" btn-group btn-group-toggle float-right" data-toggle="buttons" id='menu'>

                            

   													<label class="btn btn-sm  btn-simple active" id="0" style="background-color:rgb(198,82,232); " >
                          		<input type="radio" name="options" checked="" id='add'>
                          			<span class="d-none d-sm-block d-md-block d-lg-block d-xl-block" style="color: white;" >Adicionar</span>
                          				<span class="d-block d-sm-none"  >
                            				<i class="tim-icons icon-single-02" ></i>
                          				</span>
                        		</label>

                        		<label class="btn btn-sm  btn-simple" id="1" style="background-color:rgb(198,82,232); ">
                          		<input type="radio" class="d-none d-sm-none" name="options" id='update'>
                          			<span class="d-none d-sm-block d-md-block d-lg-block d-xl-block" style="color: white;" >Atualizar</span>
                          				<span class="d-block d-sm-none">
                            				<i class="tim-icons icon-gift-2"></i>
                          				</span>
                        		</label>

                        		<label class="btn btn-sm btn-simple" id="2" style="background-color:rgb(198,82,232); ">
                          		<input type="radio" class="d-none" name="options" id='delete'>
                          			<span class="d-none d-sm-block d-md-block d-lg-block d-xl-block" style="color: white;" >Deletar</span>
                          				<span class="d-block d-sm-none">
                            				<i class="tim-icons icon-tap-02"></i>
                          				</span>
                     	 			</label>
   													
                            
   												</div>
   												
   											</div>



   											
   										</div>
   										
   									</div>

   					<div class="card-body">
   						<div class="chart-area">
   							<div class="chartjs-size-monitor">
   								<div class="chartjs-size-monitor-expand">
   									<div class="container">        
  										<?php
                         
												echo "
													<div id='all'>

                            <div id='add'>
                              <div id='class'>
                                <h5 class='card-category'>
                                  Adicione uma nova imagem
                                </h5>
                          
																<form action='cardChange.php' method='post' enctype='multipart/form-data'>
																	<div class='form-group'>

    																<label class='btn btn-sm  btn-simple' id='1' style='background-color:rgb(198,82,232); '>
                                      <input type='file' class='d-none d-sm-none' name='imgCard' id='imgCard'>
                                        <span class='d-none d-sm-block d-md-block d-lg-block d-xl-block' style='color: white;' >
                                          Adicionar imagem
                                        </span>
                                        <span class='d-block d-sm-none'>
                                          <i class='tim-icons icon-gift-2'></i>
                                        </span>
                                    </label>
                        											
                                    <h5 class='card-category'>
                                      Adicione uma nova descrição
                                    </h5>

																		<label for='exampleFormControlTextarea1'>Descrição</label>
														    			<textarea class='form-control' id='prgCard' rows='3' style='background-color:rgb(198,82,232);' name='prgCard' >
                                      </textarea>

														  		</div>
                                  <button type='submit' class= 'btn' style='background-color:rgb(198,82,232);border-color:white;color:white;'>
                                        Adicionar
                                  </button>
																</form>

                              </div>
                            </div>";

                            require_once("Control/CardContrC.class.php");
                            $control = new CardContrC();
                            $up = $control->SelectId($_GET['id']);

                            echo"
                            <div id='update'>
                              

                              <div id='class'>
                                <h5 class='card-category'>
                                  Atualiza a imagem
                                </h5>
                              
                              <form action='cardUpdate.php' method='post' enctype='multipart/form-data'>
                                <div class='form-group'>

                                <input type='text' name='id' style='color: rgb(39,41,61);' class='form-control-plaintext' value='{$up->getId()}'>

                                  <label class='btn btn-sm  btn-simple' id='1' style='background-color:rgb(198,82,232); '>
                                    <input type='file' class='d-none d-sm-none' name='imgCard' id='imgCard'>
                                      <span class='d-none d-sm-block d-md-block d-lg-block d-xl-block' style='color: white;' >Atualizar imagem
                                      </span>
                                      <span class='d-block d-sm-none'>
                                        <i class='tim-icons icon-gift-2'></i>
                                      </span>
                                  </label>
                                              
                                  <h5 class='card-category'>
                                    Atualizar a descrição
                                  </h5>

                                      
                                  <label for='exampleFormControlTextarea1'>Descrição</label>
                                    <textarea class='form-control' id='prgCard' rows='3' style='background-color:rgb(198,82,232);' name='prgCard'>
                                    </textarea>

                                </div>

                                <button type='submit' class= 'btn' style='background-color:rgb(198,82,232);border-color:white;color:white;'>
                                  Atualizar
                                </button>
                              </form>

                              </div>
                          </div>";




                          echo "
                          <div id='delete'>
                            <h1>terceiro</h1>
                            <div id='class'>
                              <h5 class='card-category'>
                                Deletar imagem
                              </h5>

                               <form action='caChangeDe.php' method='post' enctype='multipart/form-data'>
                                <div class='form-group'>

                                  <label class='btn btn-sm  btn-simple' id='1' style='background-color:rgb(198,82,232); '>
                                    <input type='file' class='d-none d-sm-none' name='imgCard' id='imgCard'>
                                      <span class='d-none d-sm-block d-md-block d-lg-block d-xl-block' style='color: white;' >Deletar
                                      </span>
                                      <span class='d-block d-sm-none'>
                                        <i class='tim-icons icon-gift-2'></i>
                                      </span>
                                  </label>
                                              
                                  <h5 class='card-category'>
                                    Deletar a descrição
                                  </h5>

                                      
                                  <label for='exampleFormControlTextarea1'>Descrição</label>
                                    <textarea class='form-control' id='prgCard' rows='3' style='background-color:rgb(198,82,232);' name='prgCard' >
                                    </textarea>

                                </div>

                                <button type='submit' class= 'btn' style='background-color:rgb(198,82,232);border-color:white;color:white;'>
                                  Deletar
                                </button>
                              </form>

                            </div>

                          </div>

										    </div>";
															
														?>
   														
   													</div>
   													
   												</div>
   												
   											</div>
   											
   										</div>
   										
   									</div>

   									
   								</div>
   								
   							</div>
   							
   						</div>
   						
   					</div>
   					
   				</div>
   				
   			</div>
         <div style="background-color:rgb(30,30,46); " >
       <footer class="my-5 pt-5 text-muted text-center text-small" style="background-color: rgb(30,30,46);">
          <p class="mb-1">&copy; 2020 Spobrefy</p>
          <ul class="list-inline">
            <li class="list-inline-item"><a href="#">Privacidade</a></li>
            <li class="list-inline-item"><a href="#">Sobre anúcios</a></li>
            <li class="list-inline-item"><a href="cadsMusical.php">Desenvolvedor</a></li>
          </ul>
        </footer>
    </div> 

    

        <script type="text/javascript">

            function tables(a,b,c) {
              len = menu.childElementCount;
              closeall();

              for(i = 0; i < len; i++){
                menu.children[i].onclick = function(){
                  closeall();
                  all.children[this.id].style.display = "block";
                }
              }

              function closeall(){
                for(i = 0; i < all.childElementCount; i++){
                  all.children[i].style.display = "none";
                }
              }

              menu.children[c].click();
            }
            
            tables("menu","all","card");
        </script>

			
		</body>

	
		 <script src='js/bootstrap.min.js'></script>
</html>