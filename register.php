<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
   

    <title>Inscrever-se Spobrefy</title>

    
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/carousel.css" rel="stylesheet">

   
  </head>

  <body style="background-color: rgb(30,30,46); ">
    <header>
      <!--menu-->
      <nav class="navbar navbar-expand-md navbar-dark fixed-top " style="background-color: rgb(39,41,61);">
        <img  width="60px" height="60px" class="imgmel" src="imagens/logo.jpg"/>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarCollapse">

          <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
              <a class="nav-link" href="index.php">Início <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item active">
              <a class="nav-link" href="artista.html">Artistas</a>
            </li>
            
            <li class="nav-item active">
              <a class="nav-link" href="user.php">Entrar</a>
            </li>
          </ul>

        </div>
      </nav>
            <!--menu-->
    </header> 

       <div>
        <!--jumbotron-->
        <div class="jumbotron jumbotron-fluid" style="background-color: rgb(30,30,46); ">
          <div class="container">
            <div class="card" style="background-color: rgb(39,41,61) ">
             
             <div class="row">
              <div class="col-12">
                
              <div class="card-header">
                <div class="row">
                  <div class="col-sm-6 text-left">
                    <h5 class="card-category">Spobrefy</h5>
                    <h2 class="card-title">Inscreva-se</h2>
                  </div>
                  <div class="col-sm-6">
                    <div class="btn-group btn-group-toggle float-right" data-toggle="buttons" >

                      <label class="btn btn-sm  btn-simple active" id="0" style="background-color:rgb(198,82,232); " >
                        <input type="radio" name="options" checked=""  >
                        <span class="d-none d-sm-block d-md-block d-lg-block d-xl-block" style="color: white;" >Accounts</span>
                        <span class="d-block d-sm-none"  >
                          <i class="tim-icons icon-single-02" ></i>
                        </span>
                      </label>

                      <label class="btn btn-sm  btn-simple" id="1" style="background-color:rgb(198,82,232); ">
                        <input type="radio" class="d-none d-sm-none" name="options">
                        <span class="d-none d-sm-block d-md-block d-lg-block d-xl-block" style="color: white;" >Purchases</span>
                        <span class="d-block d-sm-none">
                          <i class="tim-icons icon-gift-2"></i>
                        </span>
                      </label>

                      <label class="btn btn-sm btn-simple" id="2" style="background-color:rgb(198,82,232); ">
                        <input type="radio" class="d-none" name="options">
                        <span class="d-none d-sm-block d-md-block d-lg-block d-xl-block" style="color: white;" >Sessions</span>
                        <span class="d-block d-sm-none">
                          <i class="tim-icons icon-tap-02"></i>
                        </span>
                      </label>
                    </div>
                  </div>

                </div>

              </div>
              <!--jumbotron-->

                  <div class="card-body">
                    <div class="chart-area">
                      <div class="chartjs-size-monitor">
                        <div class="chartjs-size-monitor-expand">

                          <div class="container">
                            <div class="row mx-md-n5">

                              <form action="userChange.php" method="post">

                                <div class="form-group">
                                  <div class="form-row">
                                    <div class="col">
                                      <input type="text" class="form-control" placeholder="First name">
                                    </div>
                                  </div>
                                </div>

                                  <div class="form-group">
                                    <label for="exampleInputEmail1">Email address</label>
                                      <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                                        <small id="emailHelp" class="form-text text-muted">
                                          We'll never share your email with anyone else.
                                        </small>
                                  </div>

                                  <div class="form-group">
                                    <label for="exampleInputPassword1">Password</label>
                                      <input type="password" class="form-control" id="exampleInputPassword1">
                                  </div>
                                  <div class="form-group form-check">
                                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                      <label class="form-check-label" for="exampleCheck1">
                                        Check me out
                                      </label>
                                  </div>
                                  <button type="submit" class="btn btn-primary">Submit</button>
                              </form>

                            </div>
                          </div>
                        </div>  
                      </div>
                    </div>
                        
                        <div class="chartjs-size-monitor-shrink">
                          <div class=>
                          </div>
                        
                        </div>
                           <canvas id="chartBig1" style="display: block; width: 1196px; height: 220px;" width="1196" height="220" class="chartjs-render-monitor">
                            </canvas>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      



     <div style="background-color:rgb(30,30,46); " >
       <footer class="my-5 pt-5 text-muted text-center text-small" style="background-color: rgb(30,30,46);">
          <p class="mb-1">&copy; 2020 Spobrefy</p>
          <ul class="list-inline">
            <li class="list-inline-item"><a href="#">Privacidade</a></li>
            <li class="list-inline-item"><a href="#">Sobre anúcios</a></li>
            <li class="list-inline-item"><a href="cadsMusical.php">Desenvolvedor</a></li>
          </ul>
        </footer>
    </div> 

     
    </body>
     <script src='js/bootstrap.min.js'></script>
  </html>