 <?php
	require_once("Conexao.class.php");
	require_once("Model/CardModel.class.php");
	final class CardContrC{
		 public function AddCard($add){
			# code...
			$conexao = new Conexao("Control/xuxu.ini");
			$imgTy = explode("/", $add->getTy());
			$imgTy = $imgTy[1];
			$imgBin = file_get_contents($add->getImg());
			$sql = "INSERT INTO card(img,ty,prg) VALUES(:i,:t,:p)";
			$comand = $conexao->getConexao()->prepare($sql);
			$comand->bindValue("i", $imgBin);
			$comand->bindValue("t", $imgTy);
			$comand->bindValue("p", $add->getPrg());
			if ($comand->execute()) {
				# code...
				$conexao->__destruct();
				return true;
			}else{
				$conexao->__destruct();
			}
		}

		public function UpCard($up){
			$conexao = new Conexao("Control/xuxu.ini");
			$imgTy = explode ("/", $up->getTy());
			$imgTy = $imgTy[1];
			$imgBin = file_get_contents($up->getImg());
			$sql = "UPDATE card SET img =:i, ty=:t, prg=:p WHERE id =:id;";
			$comand = $conexao->getConexao()->prepare($sql);
			$comand->bindValue("id", $up->getId());
			$comand->bindValue("i", $imgBin);
			$comand->bindValue("t", $imgTy);
			$comand->bindValue("p", $up->getPrg());
				if ($comand->execute()) {

					$conexao->__destruct();
					return true;

				}else{
					$conexao->__destruct();
				}
			
		}

		public function deleteCard($id){
			$conexao = new Conexao("Control/xuxu.ini");
			$comand = $conexao->getConexao()->prepare("DELETE FROM card WHERE id=:id");
			$comand->bindValue("id", $id);
			$comand->execute();
			$conexao->__destruct();
		}

		public function SelectCard(){
			$conexao = new Conexao("Control/xuxu.ini");
			$comand = $conexao->getConexao()->prepare("SELECT * FROM card ORDER BY id DESC");
			$comand->execute();
			$result = $comand->fetchAll();
			$lis = [];
			foreach ($result as $it) {
				$up = new CardModel();
				$up->setId($it->id);
				$up->setTy($it->ty);
				$up->setPrg($it->prg);
				array_push($lis, $up);
				# code...
			}
			$conexao->__destruct();
			return $lis;
		}

		public function SelectId($id){
			$conexao = new Conexao("Control/xuxu.ini");
			$comand = $conexao->getConexao()->prepare("SELECT * FROM card WHERE id=:id");
			$comand->bindParam("id", $id);
			$comand->execute();
			$consult = $comand->fetch();
			$model = new CardModel();
			$model->setId($consult->id);
			$model->setImg($consult->img);
			$model->setTy($consult->ty);
			$model->setPrg($consult->prg);

			$conexao->__destruct();
			return $model;


		}


	}
?>