<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
   

    <title>Spobrefy</title>

    
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/carousel.css" rel="stylesheet">

   
  </head>

  <body style="background-color: rgb(30,30,46); ">
    <header>
      <!--menu-->
      <nav class="navbar navbar-expand-md navbar-dark fixed-top " style="background-color: rgb(39,41,61);">
        <img  width="60px" height="60px" class="imgmel" src="imagens/logo.jpg"/>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarCollapse">

          <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
              <a class="nav-link" href="novo.html">Início <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item active">
              <a class="nav-link" href="artista.html">Artistas</a>
            </li>
            <li class="nav-item active">
              <a class="nav-link" href="register.php">Inscrever-se</a>
            </li>
            <li class="nav-item active">
              <a class="nav-link" href="user.php">Entrar</a>
            </li>
          </ul>

        </div>
      </nav>
            <!--menu-->
    </header>  

    <div>
      <main role="main" >
        <div id="myCarousel" class="carousel slide" data-ride="carousel" >
         <div>
          <ol class="carousel-indicators">
           <!--make the slide pass by itself-->
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
          <!--make the slide pass by itself-->
          </ol>
         </div>

        <div class="carousel-inner">
          <div class="carousel-item active">
            <img class="first-slide" src="imagens/slide1.jpeg" alt="First slide">
              <div class="container">
                <div class="carousel-caption text-left" >
                  <h1 style="font-size: 5em;font-family: arial">Spobrefy</h1>
                  <p style="font-size: 4em">Suas melhores músicas</p>
                
                  <a class="btn btn-lg" href="#" role="button" style="background-color: rgb(198,82,232);color: white;">Escutar</a></p>
                </div>
              </div>
            </div>

          <div class="carousel-item">
            <img class="second-slide" src="imagens/slide2.jpg" alt="Second slide">
              <div class="container">
                <div class="carousel-caption">
                  <h1 style="font-family: arial;font-size:4em">Artistas</h1>
                  <p style="font-family: arial;font-size: 2em">As artistas mais renomados do mundo, voçê encontra aqui suas músicas e os mais diversos .</p>
                  <p><a class="btn btn-lg" href="#" role="button" style="background-color: rgb(198,82,232);color: white;">Saiba mais</a></p>
                </div>
              </div>
          </div>

          <div class="carousel-item">
            <img class="third-slide" src="imagens/fo.jpg" alt="Third slide">
              <div class="container">
                <div class="carousel-caption text-right">
                  <h1 style="font-family: arial;font-size: 4em" >Inscrever-se</h1>
                  <h4 style="font-family: arial;font-size: 2em">Se inscreva na nossa plataforma<br>
                  e torne-se um integrante do spobrefy </h4>
                  <p>
                  <!--começo do pointpoint-->
                	 <div id="buttonContainer">
                	   <a class="btn btn-lg " href="form.html" id="push" style="background-color: rgb(198,82,232);color: white;">Inscrever-se</a>
                   </div>
                  <!--fim do pointpoint-->
                  </p>
                </div>
              </div>
          </div>
        </div>
        <!--arrow that passes the slide-->
        <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>

        <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>

       </div>
       <!--arrow that passes the slide-->
      </main>
    

    </div>

      <div>
        <!--jumbotron-->
        <div class="jumbotron jumbotron-fluid" style="background-color: rgb(30,30,46); ">
          <div class="container">
            <div class="card" style="background-color: rgb(39,41,61) ">
             
             <div class="row">
              <div class="col-12">
                
              <div class="card-header">
                <div class="row">
                  <div class="col-sm-6 text-left">
                    <h5 class="card-category">Músicas em destaques</h5>
                    <h2 class="card-title">Top 10</h2>
                  </div>
                  <div class="col-sm-6">
                    <div class="btn-group btn-group-toggle float-right" data-toggle="buttons" >

                      <label class="btn btn-sm  btn-simple active" id="0" style="background-color:rgb(198,82,232); " >
                        <input type="radio" name="options" checked=""  >
                        <span class="d-none d-sm-block d-md-block d-lg-block d-xl-block" style="color: white;" >Accounts</span>
                        <span class="d-block d-sm-none"  >
                          <i class="tim-icons icon-single-02" ></i>
                        </span>
                      </label>

                      <label class="btn btn-sm  btn-simple" id="1" style="background-color:rgb(198,82,232); ">
                        <input type="radio" class="d-none d-sm-none" name="options">
                        <span class="d-none d-sm-block d-md-block d-lg-block d-xl-block" style="color: white;" >Purchases</span>
                        <span class="d-block d-sm-none">
                          <i class="tim-icons icon-gift-2"></i>
                        </span>
                      </label>

                      <label class="btn btn-sm btn-simple" id="2" style="background-color:rgb(198,82,232); ">
                        <input type="radio" class="d-none" name="options">
                        <span class="d-none d-sm-block d-md-block d-lg-block d-xl-block" style="color: white;" >Sessions</span>
                        <span class="d-block d-sm-none">
                          <i class="tim-icons icon-tap-02"></i>
                        </span>
                      </label>
                    </div>
                  </div>


                </div>

              </div>
               <!--jumbotron-->
               
              <!--shows img-->
                <?php
                  require_once("Control/CardContrC.class.php");
                  $control = new CardContrC();
                  if ($control = $control->SelectCard()) {
                       $show = $control[count($control)-1];


                    echo "
                      <div class='card-body'>
                        <div class='chart-area'><div class='chartjs-size-monitor'>
                          <div class='chartjs-size-monitor-expand'>

                            <div class='container'>
                              <div class='row mx-md-n5'>";

                          foreach ($control as $key) {
                            # code...
                          
                            echo "
                            <div class='col px-md-3' style='margin:15px;'>
                              <div class='card' style='width: 18em;'>
                                <img src='returnImg.php?id={$key->getId()}' class='card-img-top' alt='...' '>
                                  <div class='card-body'>
                                    <p class='card-text'>{$key->getPrg()}</p>


                                  <label class='btn btn-sm  btn-simple' id='1' style='background-color:rgb(198,82,232); '>
                                    <a href='cadsMusical.php?id={$key->getId()}' style='color:white;'>Atualizar</a>
                                    <span class='d-block d-sm-none'>
                                      <i class='tim-icons icon-gift-2'></i>
                                    </span>
                                  </label>

                                 
                                  <label class='btn btn-sm  btn-simple' id='1' style='background-color:rgb(198,82,232); '>
                                     <a href='cardDelete.php?id={$key->getId()}' style='color:white;'>Deletar</a>
                                    <span class='d-block d-sm-none'>
                                      <i class='tim-icons icon-gift-2'></i>
                                    </span>
                                  </label>



                                  </div>

                                  
                              </div>
                            </div>
                            

                            

                            ";
                      }
                            echo "
                              </div>
                            </div>  
                          </div>
                            <div class='chartjs-size-monitor-shrink'>
                              <div class='>
                              </div>
                            </div>
                          </div>
                           <canvas id='chartBig1' style='display: block; width: 1196px; height: 220px;' width='1196' height='220' class='chartjs-render-monitor'>
                            </canvas>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>";

                  }else{


              
                    echo "
                        <div class='card-body'>
                          <div class='chart-area'>
                            <div class='chartjs-size-monitor'>
                              <div class='chartjs-size-monitor-expand'>

                                <div class='container'>
                                  <div class='row mx-md-n5'>
                                    <h1 class='card-title'>Ainda não há músicas adicionadas</h1>
                                  </div>
                                  </div>
                              </div>  
                            </div>
                            <div class='chartjs-size-monitor-shrink'>
                              <div class='>
                              </div>
                            </div>
                          </div>
                           <canvas id='chartBig1' style='display: block; width: 1196px; height: 220px;' width='1196' height='220' class='chartjs-render-monitor'>
                            </canvas>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
                               
                                 ";


                      }
                 
                  ?>

                  <!--shows img-->     
                      

                <!--<div class="list-group" >
                  
                  <button type="button" class="list-group-item list-group-item-action" style="background-color: rgb(39,41,61); ">
                  Cras justo odio
                  </button>
                    <button type="button" class="list-group-item list-group-item-action" style="background-color: rgb(39,41,61); ">Dapibus ac facilisis in</button>
                    <button type="button" class="list-group-item list-group-item-action" style="background-color: rgb(39,41,61); ">Morbi leo risus</button>
                    <button type="button" class="list-group-item list-group-item-action" style="background-color: rgb(39,41,61); ">Porta ac consectetur ac</button>
                    <button type="button" class="list-group-item list-group-item-action"  style="background-color: rgb(39,41,61);">Vestibulum at eros</button>
                    
                </div>-->


      <!--jumbotron-->
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div style="background-color:rgb(30,30,46); " >
       <footer class="my-5 pt-5 text-muted text-center text-small" style="background-color: rgb(30,30,46);">
          <p class="mb-1">&copy; 2020 Spobrefy</p>
          <ul class="list-inline">
            <li class="list-inline-item"><a href="#">Privacidade</a></li>
            <li class="list-inline-item"><a href="#">Sobre anúcios</a></li>
            <li class="list-inline-item"><a href="cadsMusical.php">Desenvolvedor</a></li>
          </ul>
        </footer>
    </div> 

    

  </body>

    <script src="js/app.js"></script>

    <!-- js do carrosel-->
    <script>window.jQuery || document.write('<script src="js/jquery-slim.min.js"><\/script>')</script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/holder.min.js"></script>

    
    <!-- ajax -->
    <script src="ajax.js"></script>
    <script src="teste.js"></script>

</html>
