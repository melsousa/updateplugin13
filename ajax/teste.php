<!DOCTYPE html>
<html>
<head>
	<title>teste</title>
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h3 class="text-success">AJAX</h3>
				<form >
					<div class="form-group">
						<label for="user">Username:</label>
						<input type="text" name="" id="user" class="form-control">
					</div>


					<div class="form-group">
						<label for="pwd">Password:</label>
						<input type="text" name="" id="pws" class="form-control">
					</div>

					<div class="form-group">
						<label>Choose State:</label>
						<select class="form-control" onchange="myfun(this.value)	">

							<option>Selecione o estado</option>
							<option>CE</option>
							<option>RJ</option>
							<option>SP</option>
						</select>
					</div>

					<div class="form-group">
						<label>Choose State:</label>
						<select class="form-control" id="city">
							<option>Selecione a cidade</option>
						</select>
					</div>

				</form>
			</div>
			
		</div>
		
	</div>

<script type="text/javascript">
	function myfun(data){	
		alert(data);
		var req = new XMLHttpRequest();
		req.open("GET","http://localhost/plugins13/ajax/response.php?datavalue="+data,true);
		req.send();

		req.onreadystatechange = function(){
			if (req.readyState == 4 && req.status==200) {
				document.getElementById('city').innerHTML = req.responseText;
			}
		};
	}
	
</script>
</body>
</html>